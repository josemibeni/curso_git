
# Curso de git

## Intro

![cheatsheet](./img/git-1.png)

Fuente: Imagen propia   Licencia: https://creativecommons.org/licenses/by/2.5/au/


### Crear repositorio git

Para crear un repositorio "git init". Nos creará una carpeta .git en la ruta actual, que será nuestro repositorio. Borrando dicha carpeta .git eliminaremos el repositorio.
    
    mkdir -p ~/demo_git/proyectox
    cd ~/demo_git/proyectox
    ls -la
    git init
    ls -la #Ahora tenemos una carpeta .git


### Gitignore y el area de trabajo

Como ejemplo, descargamos librerias de terceros y ejecutables (en este caso un virtualenv de python), que pueden compartir carpeta con nuestro código pero que no nos interesa que formen parte de nuestro repositorio git. Para solucionarlo usaremos el fichero .gitignore.

    python -m venv venv
    git status #Aparecen carpetas como venv
    echo -e "venv/\n*.exe\n/raiz.txt\nqualquier.txt\n/solo/solo.txt\n**\qualquier/qualquier.txt\n!excepto.txt" > .gitignore
    less .gitignore
    git status #Ya no aparece la carpeta ignorada venv, etc


### El índice o area staging

Creamos nuevos ficheros con texto de relleno, los ánadimos al indice o staging y los volvemos a modificar.

    echo -e "import os\nimport sys" > main.py
    git add main.py
    git status
    git add ma*
    git add .
    git status
    echo -e "\ndef funcion_dummy():\n  pass" >> main.py
    sed -i '2d' main.py
    git gui


### Diferencias en git

Analizamos las diferencias entre el área de trabajo y el índice.

    git status #Tenemos distintos main.sh
    git diff
    git diff -U0 #Eliminamos el contexto
    git diff --staged
    git difftool --tool-help
    git difftool --tool nvimdiff

#### Markdown

Para poder auditar las diferencias entre versiones es necesario que los ficheros sean de texto plano (no binarios). Para poder llevar el control de versiones en ficheros que no son de código, como la documentación, pero sin renunciar al formato enriquecido, se ha impuesto como estándar el uso de Markdown. 

    https://markdown-it.github.io/


### Stash

Revertimos los cambios en progreso temporalmente.

    git stash
    git stash show
    git stash pop
    git stash -a
    git stash pop


### Git commit

Creamos nuestro primer commit. El commit será una 'foto' de los cambios subidos a stagin. Las versiones propiamente dichas, sobre lo que pivota todo git. Los commits son incrementales, solo almacenan los cambios respecto a la versión anterior sin duplicar el contentido de los commits anteriores.

    git add .
    git config --global user.name 'Mi nombre'
    git config --global user.email 'mimail@mail.com'
    git config --global credential.helper store
    git commit -m 'Primer commit'
    echo -e "\ndef otra_funcion():\n  pass" >> main.py
    git status #Solo aparecen los cambios posteriores al commit
    git add .
    git commit -m "Añadida función nueva que hace no se qué"


### Auditoria de cambios y diferencias entre commits: log y diff

Etiquetamos nuestros commits y aprendemos distintos modos de visualizarlos y referenciarlos.

    git log
    git log --oneline --graph
    git tag 'v0.2' HEAD
    git tag 'v0.1' HEAD~1
    git diff HEAD~1..HEAD
    git diff HEAD..HEAD~1 #El orden importa
    git diff HEAD~1..HEAD -- main.py
    git log v0.1..v0.2
    git log HEAD~1..HEAD
    git log v0.1..HEAD
    git log --stat
    git log -p
    git blame main.py
    gitk


### Deshacer

Añadimos nuevos cambios y aprendemos a deshacerlos.

    echo -e "\ndef funcion_con_fallo():\n  print('Error!!')" >> main.py
    git add .
    git status
    git restore --staged main.py #Elimina de staging
    git status
    git restore main.py #Revierte al comit previo
    git status
    git checkout HEAD~1 -- main.py 
    git status
    git restore --staged main.py #Elimina de staging
    git restore main.py #Revierte al comit previo
    git checkout HEAD~1
    git log
    git switch -
    git revert HEAD #Crea un commit nuevo que revierte otro
    git reset HEAD~1 #Elimina el commit HEAD (cambios a area Tr)
    git restore .
    git revert HEAD #Crea un commit nuevo que revierte otro
    git reset HEAD~1 --soft #Elimina HEAD (cambios a staging)
    git restore --staged .
    git restore .
    git revert HEAD #Crea un commit nuevo que revierte otro
    git reset HEAD~1 --hard #Se pierden los cambios para siempre


### Servidores

Añadimos un servidor a nuestro repositorio local y subimos los cambios.

    git remote add origin https://gitlab.com/carlesma81/proyectox.git
    git remote add alternativo https://URL2
    git push -u origin --all #pedirá token la primera vez
    git remote show
    git remote rm origin #para eliminar el servidor
    git log origin/master
    git push origin master
    git fetch origin master
    git pull origin master
    cd ..
    git clone https://github.com/LunarVim/LunarVim
    cd LunarVim
    git remote show #ya incluye servidor a diferencia de git init


### Submodulos

Usaremos otro repositorio como submódulo. Esto es que tendremos 2 repositorios de git colgando de la misma carpeta. Uno en la carpeta principal y otro en una carpeta hija de esta. Cada repositorio tendrá sus propios remote, sus propios comits, ramas, etc y los tendremos que actualizar por separado. Pero hacer cambios en el submodulo implicara hacer commit también en el repositorio padre.

    cd ~/demo_git/proyectox
    git submodule add https://gitlab.com/carlesma81/librerias.git libs
    git add .
    git commit -m'Se añade el submodulo libs'
    less .gitmodules
    cd libs
    git log
    cd ..
    tree
    git log
    git log origin/master
    git push 
    git pull --recurse-submodules


### Ramas

![cheatsheet](./img/branches.svg)

Fuente: atlassian.com Licencia: https://creativecommons.org/licenses/by/2.5/au/

    cd libs
    git branch mejora
    git checkout mejora
    echo -e "def funcion_aux():\n  pass\n\ndef funcion_con_fallo_renovada():\n  print('Error!!' * 2)" > lib.py
    git add .
    git commit 'Se mejora una funcion que va más rápida'
    echo -e "  print('Paso 2')" >> lib.py
    git add .
    git commit 'Se añade un paso extra'
    echo -e "\n\ndef funcion_peticion_nueva():\n  pass"
    git add .
    git commit -m "Se incluye la funcionalidad nueva pedida por el cliente"
    git checkout master
    sed -i 's/Error/Hotfix/g' lib.py
    git add .
    git commit -m'Se corrige un error que hacia fallar X'
    git log
    git merge mejora #El merge siempre desde la rama que permanecerá
    git merge --abort
    git merge mejora #El merge siempre desde la rama que permanecerá
    git add .
    git commit -m'Fusion de rama merjora en master'
    git log --graph
    
![cheatsheet](./img/merge.png)

Fuente: Imagen propia Licencia: https://creativecommons.org/licenses/by/2.5/au/

![cheatsheet](./img/graph.png)

Fuente: Imagen propia Licencia: https://creativecommons.org/licenses/by/2.5/au/


### Git Bisect

Podemos detectar en que commit se dió un error por primera vez gracias a git bisect. Este, a partir de que le indiquemos un commit que falla y un commit anteior que no presenta dicho fallo, irá saltando de commit en commit donde podremos evaluar si el fallo está presente y proseguir la busqueda. Lo que aporta el comando es que no pasa por todos los commits si no que utiliza un arbol binario de busqueda para descartar rápidamente commits. Aunque estos esten en distintas ramas. Y, así, en pocos saltos localizaremos el commit responsable y podremos subsanarlo.

    git bisect start #Empezamos el proceso
    git bisect bad #Le indicamos que el commit actual (en este caso el último) presenta el fallo
    git checkout HEAD~2 #Nos desplazamos a un commit que sabemos que funciona (el proceso tiene sentido cuando hay muchísimos más commits, que no es el caso)
    git bisect good #Le indicamos que ese commit no presenta el fallo
    git bisect good #A partir de aquí git dará los salto automaticamente y le indicaremos si el commit es bueno o malo en base a los test
    git bisect bad #A los pocos saltos nos identificara el commit con los cambios que produjeron el fallo
    git reset #Terminaremos el proceso
    


